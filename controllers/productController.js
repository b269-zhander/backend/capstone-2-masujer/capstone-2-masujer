


const Product = require("../models/Product");

module.exports.addProduct = (data) => {
  if (data.isAdmin) {
    let newProduct = new Product({
      name: data.product.name,
      description: data.product.description,
      price: data.product.price
    });

    return newProduct.save()
      .then((product) => {
        return true;
      })
      .catch((error) => {
        return false;
      });
  }

  let message = Promise.resolve("User must be ADMIN to access this!");
  return message.then((value) => {
    return { value };
  });
};





// Retrieve ALL product 
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};




// Retrieve ACTIVE all product
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};




// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};






module.exports.updateProduct = (data, reqParams, reqBody) => {
  if (data.isAdmin) {
    let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
		.then((product) => {
		console.log("Product updated:", product);
		return true;
      })
		.catch((error) => {
		console.error("Error updating product:", error);
		return false;
      });
  	}

  // User is not an admin
  let message = Promise.resolve("User must be ADMIN to access this!");
  return message.then((value) => {
    console.log(value);
    return false;
  });
};





// Archive product (Admin Only)
module.exports.archiveProduct = (data, reqParams) => {
	if (data.isAdmin) {
		let updateActiveField = {
			isActive: false
		};

		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField)
			.then((product) => {
				return true;
			})
			.catch((error) => {
				return false;
			});
	};

	// User is not an admin
	let message = Promise.resolve("User must be ADMIN to access this!");
	return message.then((value) => {
		return {value};
	});
};






// Non-admin User only check-out (Create Order)
module.exports.checkoutProduct = async (productId, userId) => {
  const product = await Product.findById(productId);

  if (!product) {
    return { message: "Product not found" };
  }

  if (product.isAvailable === false) {
    return { message: "Product is not available for checkout" };
  }

  if (userId && product.checkedOutBy === userId) {
    return { message: "Product has already been checked out by this user" };
  }

  if (userId && product.checkedOutBy !== null) {
    return { message: "Product has already been checked out by another user" };
  }

  product.isAvailable = false;
  product.checkedOutBy = userId;
  await product.save();

  return { message: "Product has been checked out successfully" };
};



