

const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");

const auth = require("../auth");





// Route for Creating Product (Admin)
router.post("/create", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});




// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllproducts()
	.then(resultFromController => res.send(resultFromController));
});




// Route for retrieving active products
router.get("/active", (req, res) => {
	productController.getAllActive()
	.then(resultFromController => res.send(resultFromController));
});




// Route for retrieving specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params)
	.then(resultFromController => res.send(resultFromController));
});




// Route for updating a product (Admin)

router.put("/:productId", (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.updateProduct(data, req.params, req.body)
	.then(resultFromController => res.send(resultFromController));
});







// Route for archiving a product (Admin)

router.patch("/:productId/archive", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  productController.archiveProduct(data, req.params)
    .then(resultFromController => res.send(resultFromController));
});




// Route for checkout (Users-Only)
router.post("/checkout", auth.verify, (req, res) => {
  const userId = auth.decode(req.headers.authorization).id;
  const productId = req.body.productId;

  productController.checkoutProduct(productId, userId)
    .then(resultFromController => res.send(resultFromController));
});












module.exports = router;


