

const express = require ("express");
const mongoose = require ("mongoose");
const cors = require("cors");



const userRoute = require("./routes/userRoute");

const productRoute = require ("./routes/productRoute");

// const orderRoute = require ("./routes/orderRoute");

const app = express();



// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));




app.use("/users", userRoute);

app.use("/products", productRoute);

// app.use("/orders", orderRoute);




mongoose.connect("mongodb+srv://zmasujer:admin123@zuitt-bootcamp.kqj4tzd.mongodb.net/eCommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log("Now Connected to cloud database!"));



app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));
